﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    /// <summary>
    /// Set Partner Promo Code Limit Async Tests
    /// </summary>
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        /// <summary>
        /// Constructor
        /// </summary>
        public SetPartnerPromoCodeLimitAsyncTests() 
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// Фабричный метод инициализации данных
        /// </summary>
        /// <returns></returns>
        private SetPartnerPromoCodeLimitRequest GetRequest(int limit = 100)
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.UtcNow.AddMonths(1),
                Limit = limit
            };
        }

        /// <summary>
        /// Фабричный метод инициализации данных
        /// </summary>
        /// <returns></returns>
        private Partner GetPartner(Guid partnerId)
        {
            return new Partner()
            {
                Id = partnerId,
                IsActive = true
            };
        }
        

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFund()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var request = GetRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Partner)null);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var request = GetRequest();
            var partner = GetPartner(partnerId);
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_ClearsNumberIssuedPromoCodes()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var request = GetRequest();
            var partner = GetPartner(partnerId);
            partner.NumberIssuedPromoCodes = 10;
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>() { new PartnerPromoCodeLimit() { PartnerId = partnerId, Limit = 100, CreateDate = DateTime.UtcNow } };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<Partner>())).Returns(Task.CompletedTask);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// Если партнеру выставляется лимит, если лимит закончился, то количество не обнуляется;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasNotActiveLimit_NotClearsNumberIssuedPromoCodes()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var request = GetRequest();
            var partner = GetPartner(partnerId);
            partner.NumberIssuedPromoCodes = 10;
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>() { new PartnerPromoCodeLimit() { PartnerId = partnerId, Limit = 100, CreateDate = DateTime.UtcNow.AddDays(-10), CancelDate = DateTime.UtcNow } };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<Partner>())).Returns(Task.CompletedTask);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(partner.NumberIssuedPromoCodes);
        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_ClearsCancelDate()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var request = GetRequest();
            var partner = GetPartner(partnerId);
            partner.NumberIssuedPromoCodes = 10;
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>() { new PartnerPromoCodeLimit() { PartnerId = partnerId, Limit = 100, CreateDate = DateTime.UtcNow } };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<Partner>())).Returns(Task.CompletedTask);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert            
            partner.PartnerLimits.First().CancelDate.Should().NotBeNull();
            partner.PartnerLimits.Last().CancelDate.Should().BeNull();
        }

        /// <summary>
        /// Лимит должен быть больше 0
        /// </summary>
        /// <param name="limit"></param>
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async void SetPartnerPromoCodeLimitAsync_LimitLessThanZero_ReturnsBadRequest(int limit)
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var request = GetRequest(limit);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Build Service Collection
        /// </summary>
        /// <returns></returns>
        private IServiceProvider BuildServiceCollection()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddDbContext<DataContext>(cfg => cfg.UseInMemoryDatabase("InMemoryDb"));
            serviceCollection.AddTransient(typeof(IRepository<>), typeof(EfRepository<>));
            return serviceCollection.BuildServiceProvider();
        }

        /// <summary>
        /// Новый лимит должен быть сохранён в базу данных.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_LimitMustBeSaved_CheckInDb()
        {
            //Arrange
            var sp = BuildServiceCollection();
            var partnerId = Guid.NewGuid();
            var partner = GetPartner(partnerId);
            partner.NumberIssuedPromoCodes = 10;
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>();

            var request = GetRequest();

            var repository = sp.GetRequiredService<IRepository<Partner>>();
            await repository.AddAsync(partner);

            var controller = new PartnersController(sp.GetRequiredService<IRepository<Partner>>());

            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            var partnerInDb = await repository.GetByIdAsync(partner.Id);

            //Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();

            partnerInDb.PartnerLimits.Should().ContainSingle(x => x.PartnerId == partner.Id)
                .And.ContainSingle(x => x.Limit == request.Limit)
                .And.ContainSingle(x => x.EndDate == request.EndDate);
        }
    }
}